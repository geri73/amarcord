 <?php if(ICL_LANGUAGE_CODE == 'en'){ 
        $langLink = '/?lang=en';
    } else {
        $langLink = '';
    } 
?>
<div class="footer" id="footer">
	<?php if(!is_front_page()){ ?>
		<?php if(!is_page_template( 'tmp-partners.php' )){ ?>
		<div class="section footer-partners about-us">
			<div class="grid-container">
				<div class="cantines-slider">
				<?php 
					$terms = get_terms([
					    'taxonomy' => 'product-cantine',
					    'hide_empty' => false
					]); 
					if  ($terms) {
					        foreach ( $terms as $term) {
					?>
					<?php if (get_field('logo', $term->taxonomy.'_'.$term->term_id)) { ?>
				        <div class="single-cantine">
				        	<a href="<?php echo( get_term_link( $term ) );?>#filters"> 
				        		<img src="<?php the_field('logo_gold', $term->taxonomy.'_'.$term->term_id) ?>" alt="">
				        	</a>
				        </div> 
			       <?php 
		   				}
		                }
		              }
		            ?>
				</div>
			</div>	
		</div>
		<?php } ?>
	<?php } ?>
	<?php if(!is_page_template( 'tmp-contact.php' )&&!is_page_template( 'tmp-products.php' )){ ?>

	<section class="section get-in-touch">
		<div class="grid-container">
			<div class="grid-x">
				<div class="cell medium-6">
					<div class="title-holder">
						<div class="title no-left">
							<?php _e('Get in touch with us','amarcord'); ?>
						</div>
					</div>
					<?php if(ICL_LANGUAGE_CODE == 'en'){ ?>
					<p>We are always by your side, whenever you want. <br>You can find us on the Tirana-Durrës highway, km 13, Vora, Albania.<br>Contact us at the numbers listed below or at the email address.</p>
				    <?php } else { ?>
				        
					<p>Ndodhemi gjithmonë pranë jush, sa herë që dëshironi. <br>Na gjeni në autostradën Tiranë-Durrës, km 13, Vorë, Shqipëri. <br>Na kontaktoni në numrat e shënuar ose në adresën e email.</p>
				   <?php  } ?>
					
					<!-- <p><?php _e('We are always by your side, whenever you want. Find us on the Tirana-Durrës highway, km 13, Vora, Albania. Contact us at the numbers listed or at the email address. ','amarcord') ?></p> -->
				</div>
				<div class="cell medium-1"></div>
				<div class="cell medium-5">
					<div class="info">
						<div class="single-info">
							<img src="<?php bloginfo('template_url') ?>/img/mail.svg" alt="" class="icon">
							info@amarcord.al
						</div>
						<div class="single-info">
							<img src="<?php bloginfo('template_url') ?>/img/phone.svg" alt="" class="icon">
							+355 04 76 00091
						</div>
						<div class="single-info">
							<img src="<?php bloginfo('template_url') ?>/img/location.svg" alt="" class="icon">
							<?php _e('Tiranë-Durrës Highway, km 13, Vorë, Albania','amarcord') ?>
						</div>
						<div class="single-info">
							<img src="<?php bloginfo('template_url') ?>/img/phone.svg" alt="" class="icon">
							+355 069 20 50 095
						</div>
					</div>
					<a href="<?php echo site_url(); ?>/kontakt<?php echo $langLink; ?>" class="button"><?php _e('Contact Us','amarcord') ?> <img class="arrow" src="<?php bloginfo('template_url') ?>/img/arrow-right.svg"></a>
				</div>
			</div>
		</div>
	</section>

	<?php } ?>
	<div class="footer-inner">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell medium-3 small-12">
					<div class="footer-logo">
						<a href="<?php echo site_url(); ?><?php echo $langLink; ?>"><img src="<?php bloginfo('template_url') ?>/img/logo-text.svg" alt=""></a>
					</div>
				</div>
				<div class="cell medium-6">
					<!-- <ul class="menu">
						<li>
							<a href="">About Us</a>
						</li>
						<li>
							<a href="">Products</a>
						</li>
						<li>
							<a href="">Partners</a>
						</li>
						<li>
							<a href="">Contact Us</a>
						</li>
					</ul> -->
				</div>
				<div class="cell medium-3 small-12">
					<div class="socials">
						<a href="https://www.facebook.com/amarcordalbania" target="_blank">
							<img src="<?php bloginfo('template_url') ?>/img/fb.svg" alt="">
						</a>
						<a href="https://www.instagram.com/amarcord.al/" target="_blank">
							<img src="<?php bloginfo('template_url') ?>/img/insta.svg" alt="">
						</a>
						<a href="https://www.linkedin.com/company/amarcord-wine-and-spirits/" target="_blank">
							<img src="<?php bloginfo('template_url') ?>/img/linkedin.svg" alt="">
						</a>
					</div>
				</div>
			</div>
			<div class="grid-x grid-padding-x copyrights">
				<div class="cell medium-6 text-left"><p>© AMARCORD 2021  |  All rights reserved</p></div>
				<div class="cell medium-6 text-right"><p><a href="http://tok.al" target="_blank">Web Design & Development by TOK / Digital Agency</a></p></div>
			</div>
		</div>
	</div>

</div> 


 
    <?php wp_footer() ?>
  </body>
</html>