<?php /* Template Name: Evente */ ?>

<?php get_header(); ?>

<!-- <div class="grid-container full page-banner">
	<div class="grid-x">
		<div class="cell medium-12">
			<img src="<?php bloginfo('template_url') ?>/img/about_banner.jpg" alt="">
		</div>
	</div>
</div> -->

<section class="section events-page">
	<div class="grid-container">
		<div class="title-holder">
			<div class="title text-center">
				<?php the_title(); ?>
			</div>
		</div>
		<?php 
		$args = array(
		 	'post_type' => 'post',
	        'posts_per_page' => -1
	        );
	    $loop = new WP_Query( $args );
		if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
			<div class="single-post">
				<div class="grid-x">
					<div class="cell medium-4"> 
						<a href="<?php the_permalink(); ?>" class="featured-image">
							<?php the_post_thumbnail(); ?>
						</a>	 
					</div>
					<div class="cell medium-8">
						<div class="content-holder">
							<div class="title-holder">
								<div class="title no-decor">
									<?php the_title(); ?>
								</div>
							</div>
							<div class="post-content">
								<?php the_excerpt(); ?>
							</div>
							<a href="<?php the_permalink(); ?>" class="button"><?php _e('Read more','amarcord'); ?></a>
						</div>
					</div>
				</div>
			</div>  
		<?php endwhile;endif; ?>
	</div>
</section>
 

<?php get_footer(); ?>