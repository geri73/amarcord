<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section class="single-post-page">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-10">
				<div class="title-holder">
					<div class="title no-left">
						<?php the_title(); ?>
					</div>
				</div>
				<div class="featured-image">
					<?php the_post_thumbnail(); ?>
				</div>	
				<div class="post-content">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="cell medium-10">
				<?php 
				$images = get_field('photo_gallery');
				if( $images ):   ?>
					<div class="photo_gallery">
			            <?php 
			            foreach( $images as $image ): ?> 
							<div>
								<div class="single-image">
									<a href="<?php echo esc_url($image['url']); ?>" class="photo-holder" data-lightbox="<?php echo 'c'.$count; ?>" data-title=""><img src="<?php echo esc_url($image['url']); ?>" alt=""></a> 
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>	
 

	

<?php endwhile;endif; ?>
<?php get_footer(); ?>