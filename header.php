<!doctype html>
<html  <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title(); ?></title>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_url') ?>/img/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url') ?>/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_url') ?>/img/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url') ?>/img/fav/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_url') ?>/img/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/app.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php wp_head() ?>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&display=swap" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-Z41RZ9LRZF"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-Z41RZ9LRZF');
    </script>
</head>
<body>
<?php if(ICL_LANGUAGE_CODE == 'en'){ 
        $langLink = '/?lang=en';
    } else {
        $langLink = '';
    } 
?>

<div class="loading-screen">
    <img class="logo" src="<?php bloginfo('template_url'); ?>/img/logo.svg" width="97" height="130" alt="Amarcord Albania" />
</div>

    <div class="canvas-container">
        <div class="off-canvas-menu">
            <div class="off-canvas-inner">
                <button class="close-canvas" aria-label="Close menu" >
                  <span aria-hidden="true">&times;</span>
                </button>
                
                <ul class="menu main-menu">

                   <?php wp_nav_menu(
                    array(
                        'menu'=>'Main Menu Left'
                    )); ?>
                    <?php wp_nav_menu(
                    array(
                        'menu'=>'Main Menu Right'
                    )); ?>
                </ul>
            </div>
        </div>
    </div>

    
    <?php if (is_front_page()){ ?>
        <div class="main-logo-holder"><a href="<?php echo site_url() ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg" width="97" height="130" alt="Amarcord Albania"></a></div>
    <?php  }; ?>
    <div class="header-fix"></div>
    <div class="header" id="scroll-menu">
        <div class="main-navigation">
            <div class="grid-container">
                <div class="grid-x grid-padding-x align-center">
                    <div class="cell medium-4 right hide-for-small-only">
                         <ul class="menu main-menu  show-for-large">
                            <?php wp_nav_menu(
                                array(
                                    'menu'=>'Main Menu Left'
                            )); ?> 
                        </ul> 
                    </div>
                    <?php if (is_front_page()){ ?>
                    <div class="cell medium-2 small-6 logo-cell">
                        <div class="logo-holder front-page">
                        <?php  } else { ?>
                    <div class="cell medium-2 small-6">
                        <div class="logo-holder">
                        <?php } ?>
                            <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg" alt="Amarcord Albania"></a>
                        </div>
                    </div>
                    <div class="cell medium-4 left hide-for-small-only">
                        <ul class="menu main-menu  show-for-large">
                            <?php wp_nav_menu(
                                array(
                                    'menu'=>'Main Menu Right'
                            )); ?> 
                        </ul> 
                    </div>
                    <div class="cell medium-2 small-6 show-for-small-only">
                        <div class="hide-for-large show-canvas"><i class="fas fa-bars"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       