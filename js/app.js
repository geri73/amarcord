// $(document).foundation();



  $(".show-canvas").click(function(){
    $(".off-canvas-menu").animate({"left":"0"},300);
    $(".canvas-container").css("overflow","visible");
    $('body').toggleClass('overflow');
    $(".header").css("overflow","visible");

  });
  $(".close-canvas , .off-canvas-menu li a").click(function(){
    $(".off-canvas-menu").animate({"left":"100%"},300);
    $(".canvas-container").css("overflow","hidden");
    $('body').toggleClass('overflow');
    $(".header").css("overflow","hidden");
  });

$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
    

$('.home-slider-wrapper').slick({
  slidesToShow: 1,
  autoPlay: true,
  autoPlaySpeed: 4000,
  arrows: false,
  dots: true,
});

$('.cantines-slider').slick({
  slidesToShow: 6,
  slidesToScroll: 6,
  autoPlay: true,
  pauseOnHover: false,
  arrows: true,
  dots: false,
  swipeToScroll: true,
  responsive: [
    {
      breakpoint: 695,
      settings: {
        slidesToShow:3, 
        slidesToScroll:3 
      }
    }
  ]
});

$('.sports-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
  $('[data-goto]').removeClass('active');
  $('[data-goto="'+nextSlide+'"]').addClass('active');
});


$('[data-goto]').on('click', function(){
  gotoNumber = $(this).attr('data-goto');
  $('[data-goto]').removeClass('active');
  $(this).addClass('active');
  $('.sports-slider').slick('slickGoTo',gotoNumber);
})

  window.addEventListener("scroll", function(){
     
      // controls = $('.controls-holder');
      // controlsOffsetTop = $('.building-holder-wrapper').offset().top;
      // controlsOffsetBottom = $('.controls-holder').offset().bottom;
      // distanceTop     = (controlsOffsetTop - scrollTop);

      

      scrollTop = $(window).scrollTop();
      footer = document.getElementById('footer');
      if (scrollTop > 40) {
        $('.header').addClass('active');
        $('.header-fix').addClass('active');
        $('.main-logo-holder').addClass('active');
        $('.logo-cell').addClass('active');
      } else {
        $('.header').removeClass('active');
        $('.header-fix').removeClass('active');
        $('.main-logo-holder').removeClass('active');
        $('.logo-cell').removeClass('active');
      }

});

  $(document).ready(function(){
      if(window.pageYOffset > 39) {
        $('.header').addClass('active');
        $('.header-fix').addClass('active');
        $('.main-logo-holder').addClass('active');
        $('.logo-cell').addClass('active');
      }
      
  }) 

  $(window).on('load', function() {
       $('.loading-screen').addClass('hidden');
  });


$('.main-filters .single-filter,.new-filters .single-filter').on('click', function(){
  data = $(this).attr('data-filter');
  sub = $(this).attr('data-sub');
  $('.single-filter[data-filter='+data+']').removeClass('active');
  $(this).addClass('active');
  $('.secondary-filters .single-filter').removeClass('active');
  $('.secondary-filters').removeClass('open');
  $('.secondary-filters[data-sub='+sub+']').addClass('open');
})

$('.secondary-filters .single-filter').on('click', function(){ 
  data = $(this).attr('data-filter');
  $('.single-filter[data-filter='+data+']').removeClass('active');
  $(this).addClass('active'); 
})
$('#more-filters').on('click', function(){ 
  $('.hidden-filters').toggleClass('open');
  $(this).toggleClass('active');
})



$('[data-region]').on('click', function(){
  $('[data-region]').removeClass('active');
  $(this).addClass('active');
})
$('[data-select]').on('click', function(){
  $('[data-select]').removeClass('active');
  $(this).addClass('active');
})

$('.photo_gallery').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: true,
  dots: false,
  infinite: true
}); 




























