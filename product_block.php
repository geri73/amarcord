<div class="cell medium-3 small-6">
	<div class="single-product">
		<a href="<?php the_permalink(); ?>" class="img-holder">
			<?php the_post_thumbnail(); ?>
		</a>
		<div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
		<div class="location"><?php the_field('sub_title'); ?></div>
	</div>
</div> 