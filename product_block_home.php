<div class="single-product">
	<div class="normal-image">
		<a href="<?php the_permalink(); ?>" class="img-holder">
			<?php the_post_thumbnail(); ?>
		</a>
	</div>
	<div class="product-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
</div> 