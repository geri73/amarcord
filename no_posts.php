<?php if(ICL_LANGUAGE_CODE == 'en'){ 
        $langLink = '/?lang=en';
    } else {
        $langLink = '';
    } 
?>
<div class="cell medium-6">
	<div class="single-product">
		<div class="location"><?php _e('No products found in selected category','amarcord'); ?></div>
		<a href="<?php echo site_url();  ?>/produkte<?php echo $langLink;?>" class="button"><?php _e('Clear Filters', 'amarcord'); ?></a>
		<script>$("#load_more").hide()</script>
	</div>
</div> 