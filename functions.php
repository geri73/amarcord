<?php

add_theme_support( 'menus' );
add_theme_support( 'widgets' );
add_theme_support( 'post-thumbnails' ); 
 

 function tok_scripts() {

    wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css', '', '', false);
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap', '', '', false);
    wp_enqueue_style( 'wow-stylesheet', get_stylesheet_directory_uri() . '/scss/animate.css?v' . time(), '', '', false);
    wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/css/app.css?v' . time(), '', '', false);
    wp_enqueue_style( 'lightbox2-stylesheet', get_stylesheet_directory_uri() . '/node_modules/lightbox2/dist/css/lightbox.min.css', '', '', false);
    wp_enqueue_style( 'slick-stylesheet', get_stylesheet_directory_uri() . '/node_modules/slick-carousel/slick/slick.css', '', '', false);

    wp_deregister_script('jquery');

    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri(). '/node_modules/jquery/dist/jquery.min.js', array(), '', false);

    wp_enqueue_script( 'what-input', get_stylesheet_directory_uri(). '/node_modules/what-input/dist/what-input.min.js', array(), '', true);
    // wp_enqueue_script( 'foundation', get_stylesheet_directory_uri(). '/node_modules/foundation-sites/dist/js/foundation.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'slick-carousel', get_stylesheet_directory_uri(). '/node_modules/slick-carousel/slick/slick.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'lightbox2', get_stylesheet_directory_uri(). '/node_modules/lightbox2/dist/js/lightbox.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'wow-scripts', get_stylesheet_directory_uri(). '/js/wow.min.js?v' . time(), array(), '', true );
    wp_enqueue_script( 'tok-scripts', get_stylesheet_directory_uri(). '/js/app.js?v' . time(), array(), '', true );

}


add_action( 'wp_enqueue_scripts', 'tok_scripts' );


function product_posttype() {
	register_post_type( 'product', 
		array(
			'labels' => array(
				'name' => __( 'Products' ),
				'singular_name' => __( 'product' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
			'taxonomies'  => array( 'product_taxonomy' )
		)
	);
}
add_action( 'init', 'product_posttype' );


function product_taxonomy() {

    register_taxonomy(
        'product-category',
        'product',
        array(
            'label' => __( 'Kategoria' ),
            'rewrite' => array( 'slug' => 'product-category' ),
            'hierarchical' => true,
        )
    );
    register_taxonomy(
        'product-cantine',
        'product',
        array(
            'label' => __( 'Kantina' ),
            'rewrite' => array( 'slug' => 'product-cantine' ),
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'product-award',
        'product',
        array(
            'label' => __( 'Cmimet' ),
            'rewrite' => array( 'slug' => 'product-award' ),
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'product-region',
        'product',
        array(
            'label' => __( 'Rajoni' ),
            'rewrite' => array( 'slug' => 'product-region' ),
            'hierarchical' => true,
        )
    );


}
add_action( 'init', 'product_taxonomy' );

function get_ajax_posts_home() {
    // Query Arguments
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 3   
    ); 
    $state = $_REQUEST["state"];

    if ($_REQUEST['state']) {
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => 3 ,
            'product-region' => $state
        );
    }

    // The Query
    $ajaxposts = new WP_Query( $args );

    $response = '';

    // The Query
    if ( $ajaxposts->have_posts() ) {
        while ( $ajaxposts->have_posts() ) {
            $ajaxposts->the_post();
            $response .= get_template_part('product_block_home');
        }
    } else {
        $response .= get_template_part('no_wines');
    }

    echo $response;

    exit; // leave ajax call
}

// Fire AJAX action for both logged in and non-logged in users


add_action('wp_ajax_get_ajax_posts_home', 'get_ajax_posts_home');
add_action('wp_ajax_nopriv_get_ajax_posts_home', 'get_ajax_posts_home');



function get_ajax_posts() {
    // Query Arguments
    // $args = array(
    //     'post_type' => 'product',
    //     'posts_per_page' => 20,
    //     'order'  => 'ASC',
    //     'orderby' => 'date'
    // );
   $args = array(
        'post_type' => 'product',
        'posts_per_page' => 20,
        'product-category' => '',
        'product-cantine' => '',
        'product-region' => '',
        'order'  => 'ASC',
        'orderby' => 'date'
    );

    $post_count = 20;
    $cat = $_REQUEST["cat"];
    $state = $_REQUEST["state"];
    $cantine = $_REQUEST["cantine"];

    if ($_REQUEST['state']) {
        $state = $_REQUEST['state']; 
        if($state != 0) {
        }
            
    }
    if ($_REQUEST['count']) {
        $post_count = $_REQUEST['count'];
        if($post_count != 0) {
        }
    }
    if ($_REQUEST['cat']) {
        $cat = $_REQUEST['cat'];
        if($cat != 0) {
        }
    }
    if ($_REQUEST['cantine']) {
        $cantine = $_REQUEST['cantine'];
        if($cantine != 0) {
        }
    }

    $args['product-region'] = $state;
    $args['posts_per_page'] = $post_count;
    $args['product-category'] = $cat;
    $args['product-cantine'] = $cantine;
     
    // The Query
    $ajaxposts = new WP_Query( $args );

    $response = '';

    // The Query
    if ( $ajaxposts->have_posts() ) {
        $number = 0;
        while ( $ajaxposts->have_posts() ) {
            $ajaxposts->the_post();
            $response .= get_template_part('product_block');
             $number++;
        }
        if($post_count>$number || $number==0) {
            $response .= '<script>$("#load_more").hide()</script>';
        } else {
            $response .= '<script>$("#load_more").show()</script>';
        }
    } else {
        $response .= get_template_part('no_posts');
    }


    echo $response;

    exit; // leave ajax call
}

add_action('wp_ajax_get_ajax_posts', 'get_ajax_posts');
add_action('wp_ajax_nopriv_get_ajax_posts', 'get_ajax_posts');








add_filter('allowed_http_origins', 'add_allowed_origins');

function add_allowed_origins($origins) {
    $origins[] = 'http://www.amarcord.al';
    return $origins;
}