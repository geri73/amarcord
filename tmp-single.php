<?php /* Template Name: Single Product */ ?>

<?php get_header(); ?>

<div class="single-product-page">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-1"><br></div>
			<div class="cell medium-4">
				<div class="image-holder">
					<img src="<?php bloginfo('template_url') ?>/img/single-product.jpg" alt="">
				</div>
			</div>
			<div class="cell medium-7">
				<div class="product-content">
					<div class="title">Primitivo 12 e Mezzo</div>
					<!-- <div class="title"><?php the_title(); ?></div> -->
					<div class="sub-title">
						<?php the_field('sub_title'); ?>
					</div>
					<div class="description">
						<?php the_content(); ?>
					</div>
					<div class="infos">
						<div class="stats-holder">
							<div class="single-stat">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/location.svg" alt=""></div>
								<p><?php the_field('location'); ?></p>
							</div>
							<div class="single-stat">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/price_icon.svg" alt=""></div>
								<p><?php the_field('price'); ?> Lekë</p>
							</div>
							<div class="single-stat">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/volume_icon.svg" alt=""></div>
								<p><?php the_field('volume'); ?> cl</p>
							</div>
							<div class="single-stat">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/barrel_icon.svg" alt=""></div>
								<p><?php the_field('alcohol'); ?>% Vol</p>
							</div>
						</div>
						<div class="location-image">
							<img src="<?php bloginfo('template_url') ?>/img/location_cantine.svg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>