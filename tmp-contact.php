<?php /* Template Name: Contact */ ?>

<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>

<section class="section contact-section">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="title-holder">
					<div class="title no-left">
						<?php _e('Lets have a Glass of Wine','amarcord'); ?>
					</div>
				</div>
				<?php if(ICL_LANGUAGE_CODE == 'en'){ ?>
					<p>We are always by your side, whenever you want. <br>You can find us on the Tirana-Durrës highway, km 13, Vora, Albania.<br>Contact us at the numbers listed below or at the email address.</p>
				    <?php } else { ?>
				        
					<p>Ndodhemi gjithmonë pranë jush, sa herë që dëshironi. <br>Na gjeni në autostradën Tiranë-Durrës, km 13, Vorë, Shqipëri. <br>Na kontaktoni në numrat e shënuar ose në adresën e email.</p>
				   <?php  } ?>
				<div class="info">
					<div class="single-info">
						<img src="<?php bloginfo('template_url') ?>/img/mail.svg" alt="" class="icon">
						info@amarcord.al
					</div>
					<div class="single-info">
						<img src="<?php bloginfo('template_url') ?>/img/phone.svg" alt="" class="icon">
						+355 04 76 00091
					</div>
					<div class="single-info">
						<img src="<?php bloginfo('template_url') ?>/img/location.svg" alt="" class="icon">
						<?php _e('Tiranë-Durrës Highway, km 13, Vorë, Albania','amarcord') ?>
					</div>
					<div class="single-info">
						<img src="<?php bloginfo('template_url') ?>/img/phone.svg" alt="" class="icon">
						+355 069 20 50 095
					</div>
				</div>	
			</div>
			<div class="cell medium-6">
				<div class="form-holder">
					<?php if(ICL_LANGUAGE_CODE == 'en'){ 
				        echo do_shortcode('[ninja_form id=2]');
				    } else {
				        echo do_shortcode('[ninja_form id=1]');
				    } 
				?>
				</div>
			</div>
		</div>
	</div>
</section> 

<?php endwhile;endif; ?>
<?php get_footer(); ?>