
<?php get_header(); ?> 
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>

<div class="single-product-page">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-1"><br></div>
			<div class="cell medium-4">
				<div class="image-holder">
					<?php the_post_thumbnail(); ?>
				</div>
			</div>
			<div class="cell medium-7">
				<div class="product-content">
					<h1 class="title"><?php the_title(); ?></h1>
					<!-- <div class="title"><?php the_title(); ?></div> -->
					<div class="sub-title">
						<?php the_field('indicazione'); ?> <?php the_field('year'); ?>
					</div>
					<div class="description">
						<?php the_content(); ?>
					</div>
					<div class="infos">
						<div class="stats-holder">
							<div class="single-stat">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/location.svg" alt=""></div>
								<p><!-- ?php the_field('location'); ?> --> <?php $cantines = get_the_terms($post->ID, 'product-cantine');foreach($cantines as $cantine){echo $cantine->name;$cantineName = $cantine->name;} ?> , <?php $regions = get_the_terms($post->ID, 'product-region');foreach($regions as $region) {echo " ".$region->name;} ?></p>
							</div>
							<!-- <div class="single-stat">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/price_icon.svg" alt=""></div>
								<p><?php the_field('price'); ?> Lekë</p>
							</div> -->
							<div class="single-stat">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/volume_icon.svg" alt=""></div>
								<p><?php the_field('volume'); ?> l</p>
							</div>
							<div class="single-stat">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/barrel_icon.svg" alt=""></div>
								<p><?php the_field('alcohol'); ?> Vol</p>
							</div>
						</div>
						<div class="location-image">
							<?php $regions = get_the_terms($post->ID, 'product-region');
								foreach($regions as $region) {
									if ($region->slug == 'italy') {

									} else {
										$regSlug = $region->slug;
										$regName = $region->name;
									}
								} ?>
							<img src="<?php bloginfo('template_url') ?>/img/regions/<?php echo $regSlug; ?>.svg" alt="">
							<div class="text"><?php _e('Wine Cellar', 'amarcord') ?><br><?php echo '"'.$cantineName.'", '.$regName; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-x">
			<div class="single-product-cantine">
				<?php $post_categories = get_the_terms( $post->ID, 'product-cantine' ); ?> 
				<?php $terms = get_the_terms( $post->ID, 'product-cantine' );
                         
				if ( $terms && ! is_wp_error( $terms ) ) : 
				  
				 
				    foreach ( $terms as $term ) {
				        $cantine_name = $term->name;
				        $cantine_desc = $term->description;
				        $cantine_id = $term->term_id;
				    }
				                         
				    ?>
				  
				<?php endif; ?> 

				<?php  
					$termz = get_term( $cantine_id, 'product-cantine' );
					if (get_field('logo_gold',$termz)){
					?>
						<img class="cantine-logo" src="<?php the_field('logo_gold', $termz); ?>" alt="">
					<?php
					} else {
					?>
						<div class="title"><?php echo $cantine_name; ?></div>	
					<?php
					}
				?>
					<p class="description"><?php echo $cantine_desc; ?></p>
			</div>
		</div>
	</div>
</div>

<?php endwhile;endif; ?>
<?php get_footer(); ?>