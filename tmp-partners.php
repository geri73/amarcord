<?php /* Template Name: Partners */ ?>

<?php get_header(); ?>

<section class="section partners in-page">
	<div class="grid-container">
		<div class="grid-x"> 
			<div class="cell medium-12">
				<div class="title-holder text-center">
					<div class="title"><?php _e('Our Partners','amarcord'); ?></div>
				</div>
				<div class="title-holder">
					<div class="state-title"><?php _e('Italy','amarcord'); ?></div>
				</div>
				<div class="partners-holder">
					<div class="grid-x medium-up-5 small-up-2 grid-padding-x">
						<?php 
							$terms = get_terms([
							    'taxonomy' => 'product-cantine',
							    'hide_empty' => false
							]); 
							if  ($terms) {
							        foreach ( $terms as $term) {
							?>

							<?php if (get_field('state', $term->taxonomy.'_'.$term->term_id) == 'italy') { ?>
							<?php if (get_field('logo', $term->taxonomy.'_'.$term->term_id)) { ?>
						        <div class="cell">
						        	<div class="single-partner">
							        	<a href="<?php echo( get_term_link( $term ) );?>#filters"> 
							        		<img src="<?php the_field('logo', $term->taxonomy.'_'.$term->term_id) ?>" alt="">
							        	</a>
							        </div> 
						        </div>
					       <?php }
					   			}
				                }
				              }
				            ?>
					</div>
				</div> 
				<div class="title-holder">
					<div class="state-title"><?php _e('Spain','amarcord'); ?></div>
				</div>
				<div class="partners-holder">
					<div class="grid-x medium-up-5 small-up-2 grid-padding-x">
						<?php 
							$terms = get_terms([
							    'taxonomy' => 'product-cantine',
							    'hide_empty' => false
							]); 
							if  ($terms) {
							        foreach ( $terms as $term) {
							?>

							<?php if (get_field('state', $term->taxonomy.'_'.$term->term_id) == 'spain') { ?>
							<?php if (get_field('logo', $term->taxonomy.'_'.$term->term_id)) { ?>
						        <div class="cell">
						        	<div class="single-partner">
							        	<a href="<?php echo( get_term_link( $term ) );?>#filters"> 
							        		<img src="<?php the_field('logo', $term->taxonomy.'_'.$term->term_id) ?>" alt="">
							        	</a>
							        </div> 
						        </div>
					       <?php }
					   			}
				                }
				              }
				            ?>
					</div>
				</div> 
				<div class="title-holder">
					<div class="state-title"><?php _e('France','amarcord'); ?></div>
				</div>
				<div class="partners-holder">
					<div class="grid-x medium-up-5 small-up-2 grid-padding-x">
						<?php 
							$terms = get_terms([
							    'taxonomy' => 'product-cantine',
							    'hide_empty' => false
							]); 
							if  ($terms) {
							        foreach ( $terms as $term) {
							?>

							<?php if (get_field('state', $term->taxonomy.'_'.$term->term_id) == 'france') { ?>
							<?php if (get_field('logo', $term->taxonomy.'_'.$term->term_id)) { ?>
						        <div class="cell">
						        	<div class="single-partner">
							        	<a href="<?php echo( get_term_link( $term ) );?>#filters"> 
							        		<img src="<?php the_field('logo', $term->taxonomy.'_'.$term->term_id) ?>" alt="">
							        	</a>
							        </div> 
						        </div>
					       <?php }
					   			}
				                }
				              }
				            ?>
					</div>
				</div> 
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>