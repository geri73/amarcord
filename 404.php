<?php 
	/**
	* 404 Page (Not Found) Template
	*
	* @package WordPress
	*
	* @subpackage Amarcord
	*
	* @since Amarcord Theme Version 1.0
	*/

get_header();


 ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>

<div class="page-content">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
    		<div class="cell"><h3 class="title">404!</h3></div>
			<div class="cell">
				<p><?php _e("Can't find this page. Go back to", 'amarcord') ?> <a href="<?php echo site_url(); ?>"> <?php _e('Home', 'amarcord'); ?></a></p>
			</div>
		</div>
	</div>
</div>


<?php endwhile;endif; ?>
<?php get_footer(); ?>