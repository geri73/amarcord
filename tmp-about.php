<?php /* Template Name: About Us */ ?>

<?php get_header(); ?>

<div class="grid-container full page-banner">
	<div class="grid-x">
		<div class="cell medium-12">
			<img src="<?php bloginfo('template_url') ?>/img/about_banner.jpg" alt="">
		</div>
	</div>
</div>

<section class="section about-story">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="title-holder">
					<div class="title no-left">
						<?php _e('Amarcord Story','amarcord'); ?>
					</div>
				</div>
				<?php the_content(); ?>
			</div>
			<div class="cell medium-6">
				<div class="story-image">
					<img src="<?php bloginfo('template_url') ?>/img/about_story.svg" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about-stats">
	<div class="grid-container">
		<div class="stats-holder">
			<a href="https://amarcord.al/produkte" class="single-stat">
				<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/about_icon1.svg" alt=""></div>
				<div class="text-holder">
					<div class="title"><?php _e('Wines','amarcord'); ?></div>
					<!-- <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p> -->
				</div>
			</a>
			<a href="https://amarcord.al/produkte" class="single-stat">
				<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/about_icon2.svg" alt=""></div>
				<div class="text-holder">
					<div class="title"><?php _e('Spirits','amarcord'); ?></div>
					<!-- <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p> -->
				</div>
			</a>
			<a href="https://amarcord.al/produkte" class="single-stat">
				<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/about_icon3.svg" alt=""></div>
				<div class="text-holder">
					<div class="title"><?php _e('Others','amarcord'); ?></div>
					<!-- <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p> -->
				</div>
			</a>
		</div>
	</div>
</section>


<section class="section about-story">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="offer-image">
					<img src="<?php bloginfo('template_url') ?>/img/about_product.jpg" alt="">
				</div>
			</div>
			<div class="cell medium-6">
				<div class="title-holder">
					<div class="title no-left">
						<?php _e('All we Can Offer','amarcord'); ?>
					</div>
				</div>
				<?php the_field('second_text'); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>