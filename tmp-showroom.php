<?php /* Template Name: Show Room */ ?>

<?php get_header(); ?>

<section class="section about-story">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-10">
				<div class="title-holder">
					<div class="title no-left">
						<?php _e('Show Room','amarcord'); ?>
					</div>
				</div>
				<?php if(ICL_LANGUAGE_CODE == 'en'){ ?>
				<p>The Amarcord Wine & Spirits, located in the small province of Vora, pretty close to Tirana, was founded in 2003 by Sallaku brothers, as a result of their love and passion for wines. <br>
The company imports and distributes wines for different kind of businesses, such as restaurants, hotels and lounge bars. It offers their products to the end customer as well.The Amarcord Wine & Spirits offers its customers a wide range of products and assortments, mentioning here wines from different regions of Italy, alongside with distilled drinks and champagnes from the regions of France.</p>
			    <?php } else { ?>
			        
				<p>Kompania Amarcord Wine & Spirits, e cila ndodhet ne provincen e vogel te Vores, fare prane qytetit te Tiranes, u themelua ne vitin 2003 si rrjedhoje e dashurise dhe pasionit per veren nga vellezerit Sallaku. <br>Kompania merret me importin dhe distribucionin e vererave per kategorite e ndryshme si restorante, hotele, bar lounge, sikurse edhe per klientet privat. Ajo i propozon klienteve te saj nje game te gjere produktesh dhe asortimentesh ku spikatin vererat e rajoneve te ndryshme te Italise dhe jo vetem, si edhe pije te distiluara se bashku me shampanjen nga rajonet me ekskluzive dhe unike te Frances.</p>
			   <?php  } ?>
			</div> 
			<div class="cell medium-10">
				<?php 
				$images = get_field('photo_gallery');
				if( $images ):   ?>
					<div class="photo_gallery">
			            <?php 
			            foreach( $images as $image ): ?> 
							<div>
								<div class="single-image">
									<a href="<?php echo esc_url($image['url']); ?>" class="photo-holder" data-lightbox="<?php echo 'c'.$count; ?>" data-title=""><img src="<?php echo esc_url($image['url']); ?>" alt=""></a> 
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
 

<?php get_footer(); ?>